#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) Ashley Sommer, September 2016
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""
__version__ = '0.1.20160911'
__licence__ = 'MIT'


import asyncio
import magnet
import logging
import client
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Not using uvloop. Performance will be restricted to python's native asyncio performance.")
    pass

logging.basicConfig(level=logging.DEBUG)


def main():
    c = client.Client()
    _id, client_loop = c.start_loop()
    asyncio.ensure_future(c.get_ip_addresses_for_magnet(magnet.TEST_MAGNET_LINK),
                          loop=client_loop)
    try:
        client_loop.run_forever()
    except KeyboardInterrupt:
        pass

    client_loop.close()

if __name__ == "__main__":
    main()
