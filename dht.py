#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

import enum
from krpc import KRPCQuery, KRPCResponse, KRPCProtocol
import asyncio
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Not using uvloop. Performance will be restricted to python's native asyncio performance.")
    pass

class DHTResponse(KRPCResponse):
    @classmethod
    def decode(cls, bencoded_string):
        this = super(DHTResponse, cls).decode(bencoded_string)
        return this


class DHTQuery(KRPCQuery):
    class QueryType(enum.Enum):
        PING = b'ping'
        FIND_NODE = b'find_node'
        GET_PEERS = b'get_peers'
        ANNOUNCE_PEER = b'announce_peer'

    @classmethod
    def ping(cls, _id):
        assert isinstance(_id, (bytes, bytearray)), "Not a valid ID"
        assert len(_id) == 20, "id must be exactly 160bits (20 bytes)"
        return cls(DHTQuery.QueryType.PING, args={'id': _id})

    @classmethod
    def find_node(cls, _id, target_id):
        assert isinstance(_id, (bytes, bytearray)), "Not a valid ID"
        assert len(_id) == 20, "id must be exactly 160bits (20 bytes)"
        assert isinstance(target_id, (bytes, bytearray)), "Not a valid targetID"
        assert len(target_id) == 20, "targetID must be exactly 160bits (20 bytes)"
        return cls(DHTQuery.QueryType.FIND_NODE, args={'id': _id, 'target': target_id})

    @classmethod
    def get_peers(cls, _id, info_hash):
        assert isinstance(_id, (bytes, bytearray)), "Not a valid ID"
        assert len(_id) == 20, "id must be exactly 160bits (20 bytes)"
        assert isinstance(info_hash, (bytes, bytearray)), "Not a InfoHash"
        assert len(info_hash) == 20, "InfoHash must be exactly 160bits (20 bytes)"
        return cls(DHTQuery.QueryType.GET_PEERS, args={'id': _id, 'info_hash': info_hash})

    @classmethod
    def announce_peer(cls, _id, info_hash, token, port=None, implied_port=False):
        assert not (port is None and implied_port is False), "Use either Port, or ImpliedPort, not neither."
        assert not (port is not None and implied_port is True), "Use either Port, or ImpliedPort, not both."
        assert isinstance(_id, (bytes, bytearray)), "Not a valid ID"
        assert len(_id) == 20, "id must be exactly 160bits (20 bytes)"
        assert isinstance(info_hash, (bytes, bytearray)), "Not a InfoHash"
        assert len(info_hash) == 20, "InfoHash must be exactly 160bits (20 bytes)"
        assert isinstance(port, int) and port > 0, "Port must be an integer greater than zero"
        assert isinstance(token, (str, bytes, bytearray)), "Token must be a string or bytestring"
        assert len(token) > 0, "Got an invalid token"
        announce_dict = {'id': _id, 'info_hash': info_hash, 'token': token}
        if implied_port:
            announce_dict['implied_port'] = 1
            announce_dict['port'] = 0
        else:
            announce_dict['port'] = port
        return cls(DHTQuery.QueryType.ANNOUNCE_PEER, args=announce_dict)

    def __init__(self, query_type, t=None, args=None):
        assert isinstance(query_type, DHTQuery.QueryType), "The type must be a valid QueryType"
        super(DHTQuery, self).__init__(query_type.value, t=t, args=args)


class DHTProtocol(KRPCProtocol):
    @property
    def on_completed(self):
        return self._on_completed

    def __init__(self, loop, _id, other_id=None, dht_sent_processor=None, dht_received_processor=None, timeout=10):
        super(DHTProtocol, self).__init__(loop)
        self._id = _id
        self._other_id = other_id
        self._dht_received_processor = dht_received_processor
        self._dht_sent_processor = dht_sent_processor
        self._timeout = None if timeout is False else timeout
        self._timeout_future = None
        self._on_completed = asyncio.Event()
        self._timed_out = False
        self._errored = False

    def handle_timeout(self):
        print("Timed out after {:d} seconds.".format(int(self._timeout)))
        self._timed_out = True
        self._transport.close()
        if not self._on_completed.is_set():
            self._on_completed.set()

    def remove_timeout(self):
        if self._timeout_future is not None:
            self._timeout_future.cancel()
            self._timeout_future = None
        self._timeout = None

    def reset_timeout(self):
        if self._timeout_future is not None:
            self._timeout_future.cancel()
        if self._timeout is not None:
            t = int(self._timeout)
            print("Setting timeout for {:d} seconds.".format(t))
            self._timeout_future = self._loop.call_later(t, self.handle_timeout)
        return self._timeout_future

    def ping(self):
        ping_message = DHTQuery.ping(self._id)
        self.send(ping_message)
        return ping_message

    def find_node(self, other_id):
        find_node_message = DHTQuery.find_node(self._id, other_id)
        self.send(find_node_message)
        return find_node_message

    def find_self_node(self):
        find_node_message = DHTQuery.find_node(self._id, self._id)
        self.send(find_node_message)
        return find_node_message

    def get_peers(self, info_hash):
        get_peers_message = DHTQuery.get_peers(self._id, info_hash)
        self.send(get_peers_message)
        return get_peers_message

    def connection_made(self, transport):
        super(DHTProtocol, self).connection_made(transport)
        self.reset_timeout()

    def datagram_received(self, data, addr):
        super(DHTProtocol, self).datagram_received(data, addr)
        self.reset_timeout()
        dht_response = DHTResponse.decode(data)
        asyncio.ensure_future(self._dht_received_processor(dht_response), loop=self._loop)
        return dht_response

    def error_received(self, exc):
        super(DHTProtocol, self).error_received(exc)
        self._errored = True
        self.remove_timeout()
        if not self._on_completed.is_set():
            self._on_completed.set()

    def connection_lost(self, exc):
        super(DHTProtocol, self).connection_lost(exc)
        self.remove_timeout()
        if not self._on_completed.is_set():
            self._on_completed.set()
