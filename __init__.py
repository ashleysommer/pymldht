#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""
__version__ = '0.1.20160914'
__licence__ = 'MIT'
__author__ = 'Ashley Sommer'
__copyright__ = 'Copyright 2016, Ashley Sommer'
__credits__ = ["Ashley Som,er"]
__maintainer__ = "Ashley Sommer"
__email__ = "ashleysommer@gmail.com"
__status__ = "Development"
