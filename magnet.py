#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

from urllib import parse as urlparse

TEST_MAGNET_LINK = \
    '''magnet:?xt=urn:btih:3d56bd42ce26355352a80912d6ddc74651465d4b&dn=Suicide+Squad%3A+The+Album+%282016%29'''


def process_xt_link(xt_link):
    """
    :param xt_link: str
    :return:
    """
    assert str(xt_link).startswith("xt="), "Not a valid xt string."
    parts = xt_link.split('=', 1)
    parts = parts[1].split(':')
    xt_dict = {}
    for part in parts:
        if part == "urn":
            xt_dict["is_urn"] = True
        elif part == "btih":
            xt_dict["is_btih"] = True
        else:
            keys = xt_dict.keys()
            if "is_urn" in keys and "is_btih" in keys:
                xt_dict['btih'] = bytes(bytearray.fromhex(part))
            else:
                raise NotImplementedError("Don't know how to process: {:s}".format(part))
    return xt_dict


def process_display_name(dn_string):
    """
    :param dn_string: str
    :return:
    """
    assert str(dn_string).startswith("dn="), "Not a valid Display Name (dn) string."
    parts = dn_string.split('=', 1)
    parts = parts[1].split(':')
    name = parts[0]
    name = urlparse.unquote_plus(name)
    return name


def process_magnet_link(magnet_link):
    assert magnet_link.startswith('magnet:'), "Magnet links must start with \"magnet:\""
    parts = magnet_link.split('?', 1)
    assert len(parts) > 1, "Magnet link must have a question mark delimiter near the start."
    parts = parts[1].split('&')
    magnet_dict = {}
    for part in parts:
        print(part)
        if part.startswith("xt="):
            magnet_dict['xt'] = process_xt_link(part)
        elif part.startswith("dn="):
            magnet_dict['dn'] = process_display_name(part)
        elif part.startswith("tr="):
            pass  # ignore trackers. we don't need them.

    return magnet_dict


if __name__ == "__main__":
    process_magnet_link(TEST_MAGNET_LINK)
