#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

from os import urandom
import asyncio
import distance
from dht import DHTProtocol, DHTQuery
from nodes import NodeManager
import magnet
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Not using uvloop. Performance will be restricted to python's native asyncio performance.")
    pass


BOOTSTRAP_NODE_NAME = "router.bittorrent.com"
BOOTSTRAP_NODE_PORT = 6881

class Client(object):
    @asyncio.coroutine
    def process_received_response_dht_find_node(self, dht_message, dht_response):
        """
        :param dht_response: DHTResponse
        :return:
        """
        try:
            respose_dict = dht_response['r']
        except KeyError:
            return
        # try:
        #     ip = dht_response['ip']
        #     ip_addr = struct.unpack("!BBBB", ip[0:4])
        #     port = struct.unpack("!H", ip[4:6])[0]
        # except KeyError:
        #     pass
        try:
            nodes = respose_dict['nodes']
        except KeyError:
            return
        nodes = self.node_manager.uncompact_nodes(self._id, nodes)
        if 'ping_before_add' in dht_message.keys():
            ping_before_add = dht_message['ping_before_add']
        else:
            ping_before_add = False
        yield from self.node_manager.add_nodes_to_buckets(self, nodes, ping_before_add=ping_before_add)
        return

    @asyncio.coroutine
    def process_received_response_dht_get_peers(self, dht_message, dht_response):
        """
        :param dht_response: DHTResponse
        :return:
        """
        try:
            respose_dict = dht_response['r']
        except KeyError:
            return
        # try:
        #     ip = dht_response['ip']
        #     ip_addr = struct.unpack("!BBBB", ip[0:4])
        #     port = struct.unpack("!H", ip[4:6])[0]
        # except KeyError:
        #     pass
        try:
            nodes = respose_dict['nodes']
        except KeyError:
            nodes = []

        if len(nodes) > 0:
            nodes = self.node_manager.uncompact_nodes(self._id, nodes)
            if 'ping_before_add' in dht_message.keys():
                ping_before_add = dht_message['ping_before_add']
            else:
                ping_before_add = False
            yield from self.node_manager.add_nodes_to_buckets(self, nodes, ping_before_add=ping_before_add)
        try:
            values = respose_dict['values']
            print(values)
        except KeyError:
            values = False

        if values is not False and len(values) > 0:
            values = self.node_manager.uncompact_ip_values(self._id, values)

        try:
            token = respose_dict['token']
        except KeyError:
            token = False

        return {'values': values, 'token': token}


    @asyncio.coroutine
    def process_received_response_dht_ping(self, dht_message, dht_response):
        """
        :param dht_response: DHTResponse
        :return:
        """
        pass


    @asyncio.coroutine
    def process_received_dht_message(self, dht_received):
        """
        :param dht_received: KRPCMessage
        :return:
        """
        transaction_id = dht_received['t']
        delete_trans = False

        def delete_history_trans():
            if delete_trans is False:
                return
            with (yield from self.message_history_lock):
                del self.message_history[delete_trans]
        sent_message = None
        with (yield from self.message_history_lock):
            try:
                sent_message = self.message_history[transaction_id]
            except KeyError:
                pass

        if sent_message is not None:
            delete_trans = transaction_id
            continue_processing = True
            if 'on_response_before_process' in sent_message:
                continue_processing = yield from sent_message['on_response_before_process']()
            process_result = None
            if continue_processing is False:
                pass
            else:
                if 'y' not in sent_message.keys():  # looks like an invalid message. Ignore it, and delete it.
                    pass
                if sent_message['y'] == b'q':  # good, it is a KRPC query. So is probably a DHTQuery
                    if 'q' not in sent_message.keys():
                        pass
                    elif sent_message['q'] == DHTQuery.QueryType.FIND_NODE.value:
                        process_result = yield from self.process_received_response_dht_find_node(sent_message,
                                                                                                 dht_received)
                    elif sent_message['q'] == DHTQuery.QueryType.GET_PEERS.value:
                        process_result = yield from self.process_received_response_dht_get_peers(sent_message,
                                                                                                 dht_received)
                elif sent_message['y'] == b'r':  # we sent a response. don't want to process the response to a response
                    pass
                elif sent_message['y'] == b'e':  # we send at exception. don't want to process the response to an exception
                    pass
                else:
                    pass
            if 'on_response_after_process' in sent_message:
                sent_message['on_response_after_process'](process_result)
            delete_history_trans()

    @asyncio.coroutine
    def save_sent_dht_message(self, dht_message):
        transaction_id = dht_message['t']
        with (yield from self.message_history_lock):
            self.message_history[transaction_id] = dht_message

    class DHTHandlerProtocol(DHTProtocol):
        def __init__(self, client, **kwargs):
            super(Client.DHTHandlerProtocol, self).__init__(client.loop, client._id,
                                                     dht_sent_processor=client.save_sent_dht_message,
                                                     dht_received_processor=client.process_received_dht_message,
                                                     **kwargs)
            self._client = client

    class JustCheckPing(DHTHandlerProtocol):
        def connection_made(self, transport):
            super(Client.JustCheckPing, self).connection_made(transport)
            message_sent = self.ping()
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def datagram_received(self, data, addr):
            response = super(Client.JustCheckPing, self).datagram_received(data, addr)
            self._transport.close()

    class BootstrapProtocol0(DHTHandlerProtocol):
        def __init__(self, client, next_bootstrap_step, **kwargs):
            super(Client.BootstrapProtocol0, self).__init__(client, **kwargs)
            self.next_bootstrap_step = next_bootstrap_step

        def connection_made(self, transport):
            super(Client.BootstrapProtocol0, self).connection_made(transport)
            message_sent = self.find_self_node()
            message_sent['on_response_after_process'] = lambda result: asyncio.ensure_future(
                                                        self.next_bootstrap_step(result, 0))
            message_sent['ping_before_add'] = Client.JustCheckPing
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def datagram_received(self, data, addr):
            response = super(Client.BootstrapProtocol0, self).datagram_received(data, addr)
            self._transport.close()


    class BootstrapProtocol1(DHTHandlerProtocol):
        def __init__(self, client, **kwargs):
            super(Client.BootstrapProtocol1, self).__init__(client, **kwargs)
            self.is_complete = asyncio.Event()
            asyncio.ensure_future(self.done_handler())

        @asyncio.coroutine
        def done_handler(self):
            yield from self._on_completed.wait()
            print("Unlocking and closing this.")
            self.is_complete.set()
            self._transport.close()

        def find_other_node(self):
            message_sent = self.find_node(self._other_id)
            message_sent['on_response_after_process'] = lambda result: self._on_completed.set()
            message_sent['ping_before_add'] = Client.JustCheckPing
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def find_my_node(self):
            self._timeout = 6
            self.reset_timeout()
            message_sent = self.find_self_node()
            message_sent['on_response_after_process'] = lambda result: self.find_other_node()
            message_sent['ping_before_add'] = Client.JustCheckPing
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def connection_made(self, transport):
            self._timeout = 3 #temporary 2000ms timeout for Ping request
            super(Client.BootstrapProtocol1, self).connection_made(transport)
            message_sent = self.ping()
            message_sent['on_response_after_process'] = lambda result: self.find_my_node()
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def datagram_received(self, data, addr):
            response = super(Client.BootstrapProtocol1, self).datagram_received(data, addr)

        def error_received(self, exc):
            super(Client.BootstrapProtocol1, self).error_received(exc)
            self.is_complete.set()


    class SimpleGetPeers(DHTHandlerProtocol):
        def __init__(self, client, **kwargs):
            super(Client.SimpleGetPeers, self).__init__(client, **kwargs)
            self.is_complete = asyncio.Event()
            self.result = None
            self.got_ping = False
            asyncio.ensure_future(self.done_handler())

        def save_result(self, result):
            self.result = result
            return True

        @asyncio.coroutine
        def done_handler(self):
            yield from self._on_completed.wait()
            print("Unlocking and closing this.")
            self.is_complete.set()
            self._transport.close()

        def do_get_peers(self):
            self.got_ping = True
            self._timeout = 6
            self.reset_timeout()
            message_sent = self.get_peers(self._other_id)
            message_sent['on_response_after_process'] = lambda result: self.save_result(result) and \
                                                                       self._on_completed.set()
            message_sent['ping_before_add'] = Client.JustCheckPing
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def connection_made(self, transport):
            self._timeout = 3 #temporary 3000ms timeout for Ping request
            super(Client.SimpleGetPeers, self).connection_made(transport)
            message_sent = self.ping()
            message_sent['on_response_after_process'] = lambda result: self.do_get_peers()
            asyncio.ensure_future(self._dht_sent_processor(message_sent), loop=self._loop)

        def datagram_received(self, data, addr):
            response = super(Client.SimpleGetPeers, self).datagram_received(data, addr)

        def error_received(self, exc):
            super(Client.SimpleGetPeers, self).error_received(exc)
            self.is_complete.set()


    @asyncio.coroutine
    def bootstrap0(self, loop):
        print("Attempting bootstrap. Querying entry point (bootstrap node).")
        bootstrap_connect = loop.create_datagram_endpoint(
            lambda: Client.BootstrapProtocol0(self, next_bootstrap_step=self.bootstrap1),
            remote_addr=(BOOTSTRAP_NODE_NAME, BOOTSTRAP_NODE_PORT))
        transport, protocol = yield from bootstrap_connect

        #transport.close()

    @asyncio.coroutine
    def bootstrap1(self, process_result, iter):
        print("Entring bootstrap iteration {:d}".format(iter))
        smallest_bucket = None
        tasks = None
        with (yield from self.node_manager.buckets_lock):
            if len(self.node_manager.buckets) >= 6:  # 6 buckets should be plenty for bootstrapping purposes
                self.bootstrapped_event.set()
                return
            for b in self.node_manager.buckets:
                if b.smallest:
                    smallest_bucket = b
                    break
            if smallest_bucket is not None:
                tasks = [
                    (lambda _n: self.loop.create_datagram_endpoint(
                    lambda: Client.BootstrapProtocol1(self, other_id=_n),
                    remote_addr=(smallest_bucket._nodes[_n].ip_address, smallest_bucket._nodes[_n].port)))(n)
                    for n in smallest_bucket._nodes
                ]
        if tasks is not None:
            task_items = yield from asyncio.gather(*tasks)
            print("Got x nodes in our closest bucket. Recursively querying them.".format(len(tasks)))
            wait_tasks = [p.is_complete.wait() for _, p in task_items]
            done = yield from asyncio.gather(*wait_tasks)
            if iter >= 4: #is 4 iterations enough?
                self.bootstrapped_event.set()
                return
            asyncio.ensure_future(self.bootstrap1(process_result=None, iter=iter+1))

    @asyncio.coroutine
    def get_peers_traversal(self, info_hash, hash_distance=None, category=None, iterate_depth=6):
        import operator
        if hash_distance is None:
            hash_distance = distance.calculate_distance(self._id, info_hash)
        if category is None:
            category = distance.categorize_distance(hash_distance)
        iter = 0
        found_values = []
        min_nodes_to_use = 10
        while iter <= iterate_depth:
            bucket_category_distances = {}
            closest_bucket = None
            nodes_to_use = {}
            with (yield from self.node_manager.buckets_lock):
                for i, b in enumerate(self.node_manager.buckets):
                    highest, lowest = b.highest_and_lowest
                    hdif = abs(category-highest)
                    ldif = abs(category-lowest)
                    smallest_dif = min(hdif, ldif)
                    bucket_category_distances[i] = smallest_dif
                sorted_buckets = sorted(bucket_category_distances.items(), key=operator.itemgetter(1))
                closest_bucket = self.node_manager.buckets[sorted_buckets[0][0]]
                if closest_bucket is None:
                    raise RuntimeError("Cannot find a bucket of nodes to use.")
                if len(closest_bucket._nodes) >= min_nodes_to_use:
                    nodes_to_use = closest_bucket._nodes
                else:
                    nodes_to_use.update(closest_bucket._nodes)
                    for i in range(1, len(sorted_buckets)):
                        next_bucket = self.node_manager.buckets[sorted_buckets[i][0]]
                        nodes_to_use.update(next_bucket._nodes)
                        if len(nodes_to_use) >= min_nodes_to_use:
                            break

            tasks = (
                (lambda _n: self.loop.create_datagram_endpoint(
                    lambda: Client.SimpleGetPeers(self, other_id=info_hash),
                    remote_addr=(nodes_to_use[_n].ip_address, nodes_to_use[_n].port)))(n)
                for n in nodes_to_use
            )
            task_items = yield from asyncio.gather(*tasks)
            print(len(task_items))
            wait_tasks = [p.is_complete.wait() for _, p in task_items]
            done = yield from asyncio.gather(*wait_tasks)
            for _, p in task_items:
                try:
                    result = p.result
                    assert result is not None
                    values = result['values']
                    if values is not False and values is not None and len(values) > 0:
                        found_values.extend(values)
                except (AttributeError, KeyError, AssertionError):
                    continue
            if len(found_values) > 0:
                break
            iter += 1
        return found_values

    @asyncio.coroutine
    def get_ip_addresses_for_magnet(self, magnet_link):
        mag = magnet.process_magnet_link(magnet_link)
        assert 'xt' in mag.keys(), "The magnet_link MUST have an xt parameter to be able to find it."
        if 'btih' in mag['xt'].keys():
            info_hash = mag['xt']['btih']
        else:
            raise NotImplementedError("Need to process other hash types here.")
        distance_to_hash = distance.calculate_distance(self._id, info_hash)
        category = distance.categorize_distance(distance_to_hash)
        yield from self.bootstrapped_event.wait()
        res = yield from self.get_peers_traversal(info_hash, hash_distance=distance_to_hash, category=category)
        print(res)

    def start_loop(self):
        asyncio.ensure_future(self.node_manager.node_bucket_manager(), loop=self.loop)
        asyncio.ensure_future(self.bootstrap0(self.loop), loop=self.loop)
        return self._id, self.loop

    def __init__(self):
        self.loop = asyncio.get_event_loop_policy().get_event_loop()
        self.loop.set_debug(True)
        self._id = urandom(20)
        self.bootstrapped_event = asyncio.Event(loop=self.loop)
        self.message_history_lock = asyncio.Lock(loop=self.loop)
        self.message_history = {}
        self.node_manager = NodeManager(self.loop)
