#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

import io

example_decode = b'''d3:bar4:spam3:fooi42ee'''
example_encode = {'bar': 'spam', 'foo': 42}
numbers_list = [b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'-']


def decode_integer(reader):
    bytes_array = bytearray()
    next_byte = reader.read1(1)
    while next_byte != b'e':
        assert next_byte in numbers_list, "Integer values MUST be ascii digits 0-9 or minus(-)"
        bytes_array.append(next_byte[0])
        next_byte = reader.read1(1)
    if len(bytes_array) < 1:
        return 0
    byte_string = bytes(bytes_array)
    integer = int(byte_string, base=10)
    return integer


def decode_string(reader, first_digit, encoding='utf-8'):
    length_bytes_array = bytearray(first_digit)
    next_byte = reader.read1(1)
    while next_byte != b':':
        assert next_byte in numbers_list, "Integer values MUST be ascii digits 0-9 or minus(-)"
        length_bytes_array.append(next_byte[0])
        next_byte = reader.read1(1)
    length_string = bytes(length_bytes_array)
    length = int(length_string, base=10)
    if length < 1:
        return ""
    string = reader.read(length)
    if encoding is None:
        return string
    return bytes(string).decode(encoding=encoding)


def decode_list(reader, encoding='utf-8'):
    return_list = []
    next_element = decode_element(reader, encoding=encoding)
    while next_element is not None:
        return_list.append(next_element)
        next_element = decode_element(reader, encoding=encoding)
    return return_list


def decode_dictionary(reader, encoding='utf-8'):
    return_dict = {}

    next_key = decode_element(reader, encoding='utf-8')  # hopefully all keys are going to be valid utf-8
    while next_key is not None:
        assert isinstance(next_key, str), "Key {:s} must be a string.".format(str(next_key))
        value = decode_element(reader, encoding=encoding)
        assert value is not None, "Did not get a value for key {:s}".format(next_key)
        return_dict[next_key] = value
        next_key = decode_element(reader, encoding='utf-8')  # get the next key in utf-8 again.
    return return_dict


def decode_element(reader, encoding='utf-8'):
    first = reader.read1(1)
    if first == b'e':
        return None
    if first == b'i':
        return decode_integer(reader)
    elif first == b'l':
        return decode_list(reader, encoding=encoding)
    elif first == b'd':
        return decode_dictionary(reader, encoding=encoding)
    elif first in numbers_list:
        return decode_string(reader, first, encoding=encoding)
    else:
        raise RuntimeError("Letter '{:s}' is not a valid Bencode marker.".format(str(first)))


def decode(bencoded_string, encoding='utf-8'):
    bytes_io = io.BytesIO(bencoded_string)
    reader = io.BufferedReader(bytes_io)
    reader.seek(0)
    e = decode_element(reader, encoding=encoding)
    return e


def encode_integer(integer):
    string = "i{:d}e".format(integer)
    return bytes(string, encoding='ascii')


def encode_string(string, encoding='utf-8'):
    if isinstance(string, bytes) and encoding is None:
        byte_string = string  # skip the decode
    else:
        assert encoding is not None, "You cannot pass 'None' to encode when you are passing in Unicode strings."
        byte_string = bytes(string, encoding=encoding)
    return encode_bytes(byte_string)


def encode_bytes(byte_string_or_array):
    if isinstance(byte_string_or_array, bytearray):
        bytestring = bytes(byte_string_or_array)  # convert to bytes
    else:
        bytestring = byte_string_or_array  # don't convert
    length = len(bytestring)
    length_string = "{:d}".format(length)
    length_bytestring = bytes(length_string, encoding='ascii')
    return bytes.join(b':', [length_bytestring, bytestring])


def encode_dictionary(dictionary, encoding='utf-8'):
    bytestring_array = [b'd']
    for k in sorted(dictionary):
        key_string = str(k)
        key_bytestring = encode_string(key_string, encoding='utf-8')  # always encode key strings as utf-8
        bytestring_array.append(key_bytestring)
        value = dictionary[k]
        value_repr = encode(value, encoding=encoding)
        bytestring_array.append(value_repr)
    bytestring_array.append(b'e')
    return bytes.join(b'', bytestring_array)


def encode_list(python_list, encoding='utf-8'):
    bytestring_array = [b'l']
    # for value in sorted(python_list):
    for value in python_list:  # Don't sort the list. They must appear in the natural array order.
        value_repr = encode(value, encoding=encoding)
        bytestring_array.append(value_repr)
    bytestring_array.append(b'e')
    return bytes.join(b'', bytestring_array)


def encode(python_object, encoding='utf-8'):
    if python_object is None:
        return b'e'
    if isinstance(python_object, int):
        return encode_integer(python_object)
    elif isinstance(python_object, str):
        return encode_string(python_object, encoding=encoding)
    elif isinstance(python_object, (bytes, bytearray)):
        return encode_bytes(python_object)
    elif isinstance(python_object, dict):
        return encode_dictionary(python_object, encoding=encoding)
    elif isinstance(python_object, (list, tuple)):
        return encode_list(python_object, encoding=encoding)
    else:
        raise RuntimeError("That type of object cannot be Bencoded")


if __name__ == "__main__":
    decode(example_decode)
    encode(example_encode)
