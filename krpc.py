#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

import asyncio
import bencode
import os
import enum
import logging
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Not using uvloop. Performance will be restricted to python's native asyncio performance.")
    pass

logger = logging.getLogger(__name__)


class KRPCMessage(dict):
    """
    A KRPC message is a single dictionary with two keys common to every message and additional keys depending on the
    type of message. Every message has a key "t" with a string value representing a transaction ID. This transaction ID
    is generated by the querying node and is echoed in the response, so responses may be correlated with multiple
    queries to the same node. The transaction ID should be encoded as a short string of binary numbers, typically 2
    octets are enough as they cover 2^16 outstanding queries. The other key contained in every KRPC message is "y" with
    a single character value describing the type of message. The value of the "y" key is one of "q" for query, "r" for
    response, or "e" for error.
    """

    @classmethod
    def decode(cls, bencoded_string):
        assert isinstance(bencoded_string, bytes), "For now, Bencoded strings should always be bytes type."
        python_object = bencode.decode(bencoded_string, encoding=None)  # Use None to get strings as bytestrings
        assert isinstance(python_object, dict), "The Bencoded string did not decode to a dict object."
        # TODO, figure how how to determine which bytestrings can be decoded to utf-8.
        return cls.from_dict(python_object)


    @classmethod
    def from_dict(cls, dictionary):
        try:
            message_type = dictionary.pop('y')
        except KeyError:
            raise RuntimeError("Dictionary must contain message type ('y').")
        t = dictionary.pop('t', None)
        return KRPCMessage(message_type, t, other=dictionary)

    def encode(self):
        return bencode.encode_dictionary(self)

    def __init__(self, message_type, t=None, other=None):
        if isinstance(message_type, str):
            message_type = message_type.encode(encoding='ascii')
        assert message_type in [b'q', b'r', b'e'], "Message Type must be either 'q', 'r', or 'e'."
        self['y'] = message_type
        if t is None:
            t = os.urandom(2)
        self['t'] = t
        if other is not None:
            self.update(other)


class KRPCQuery(KRPCMessage):
    def __init__(self, name, t=None, args=None, **kwargs):
        assert name is not None and len(name) > 0, "Query must contain a valid name."
        if args is None and len(kwargs) > 0:
            args = kwargs
        message_type = b'q'
        message_dict = {'q': name, 'a': args}
        super(KRPCQuery, self).__init__(message_type, t=t, other=message_dict)


class KRPCResponse(KRPCMessage):
    def __init__(self, t=None, args=None, **kwargs):
        if args is None and len(kwargs) > 0:
            args = kwargs
        message_type = b'r'
        message_dict = {'r': args}
        super(KRPCResponse, self).__init__(message_type, t=t, other=message_dict)


class KRPCError(KRPCMessage):
    class ErrorCodes(enum.IntEnum):
        GENERIC = 201
        SERVER = 202
        PROTOCOL = 203
        METHOD_UNKNOWN = 204

    ERROR_STRINGS = {
        ErrorCodes.GENERIC: "A Generic error occurred.",
        ErrorCodes.SERVER: "A Server error occurred.",
        ErrorCodes.PROTOCOL: "A Protocol error occurred.",
        ErrorCodes.METHOD_UNKNOWN: "Unknown Method."
    }

    def __init__(self, error_code, error_string=None, t=None, extra=None, allow_unreal_codes=False):
        error_code = int(error_code)
        try:
            code = KRPCError.ErrorCodes(error_code)
        except Exception as e:
            print(repr(e))
            if not allow_unreal_codes:
                raise RuntimeError("Cannot use that error code. It is not a real error code.")
            code = None
        message_type = b'e'
        if (error_string is None or len(error_string) < 1) and code is not None:
            error_string = KRPCError.ERROR_STRINGS[code.value]
        if error_string is None or len(error_string) > 0:
            logger.warn("Could not determine the correct error string.")
            error_string = "Unknown error."
        error_list = [error_code, error_string]
        if extra is not None:
            error_list.extend(extra)
        message_dict = {'e': error_list}
        super(KRPCError, self).__init__(message_type, t=t, other=message_dict)


class KRPCProtocol(asyncio.DatagramProtocol):
    def __init__(self, loop):
        self._loop = loop
        self._transport = None

    def send(self, krpc_message):
        assert isinstance(krpc_message, KRPCMessage), "the send function takes only a 'krpc_message'"
        assert self._transport is not None, "The transport must not be None."
        print('Send:', krpc_message)
        print("Encoding: ", krpc_message.encode())
        self._transport.sendto(krpc_message.encode())

    def connection_made(self, transport):
        self._transport = transport

    def datagram_received(self, data, addr):
        print("Received:", data)
        #self.transport.close()

    def error_received(self, exc):
        print("Error received: {:s}".format(str(exc)))
        self._transport.close()

    def connection_lost(self, exc):
        print("Socket closed. {:s}".format(str(exc)))
        #loop = asyncio.get_event_loop()
        #loop.stop()

# def main():
#     loop = asyncio.get_event_loop()
#     message = KRPCError(209)
#     connect = loop.create_datagram_endpoint(
#         lambda: KRPCProtocol(loop),
#         remote_addr=('127.0.0.1', 9999))
#     transport, protocol = loop.run_until_complete(connect)
#     loop.run_forever()
#     transport.close()
#     loop.close()
#
# if __name__ == "__main__":
#     main()
