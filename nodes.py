#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""

import struct
import time
import distance
import asyncio
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print("Not using uvloop. Performance will be restricted to python's native asyncio performance.")
    pass

MAX_NODES_PER_BUCKET = 20


class NodeManager(object):
    class OtherNode(object):
        @property
        def ip_address(self):
            return self._ip_address

        @ip_address.setter
        def ip_address(self, value):
            raise NotImplementedError("Cannot set the IP address after creation.")

        @property
        def category(self):
            return self._category

        @category.setter
        def category(self, value):
            raise NotImplementedError("Cannot set the category after creation.")

        @property
        def distance(self):
            return self._distance

        @distance.setter
        def distance(self, value):
            raise NotImplementedError("Cannot set the distance after creation.")

        @property
        def id(self):
            return self._id

        @id.setter
        def id(self, value):
            raise NotImplementedError("Cannot set the ID after creation.")

        @property
        def port(self):
            return self._port

        @port.setter
        def port(self, value):
            raise NotImplementedError("Cannot set the port after creation.")

        def update_last_seen(self):
            self._last_seen = int(time.time())

        def __init__(self, _id, other_id, ip_address, port):
            t = int(time.time())
            self._distance = distance.calculate_distance(_id, other_id)
            self._category = distance.categorize_distance(self._distance)
            self._id = other_id
            self._ip_address = ip_address
            self._port = port
            self._inserted = t
            self._last_seen = t

    class Bucket(object):
        @property
        def smallest(self):
            return self._smallest

        @smallest.setter
        def smallest(self, value):
            self._smallest = bool(value)

        def append(self, node):
            node_id = node.id
            if node_id in self._nodes.keys():
                self._nodes[node_id].update_last_seen()
            else:
                cat = node.category
                self._nodes[node_id] = node
                if cat < self._actual_lowest:
                    self._actual_lowest = cat
                if cat > self._actual_highest:
                    self._actual_highest = cat

        def __len__(self):
            return len(self._nodes)

        @property
        def range(self):
            return self._upper_cat, self._lower_cat  # tuple

        @range.setter
        def range(self, value):
            raise NotImplementedError("You cannot set the range.")

        @property
        def highest_and_lowest(self):
            if len(self._nodes) < 1:
                _range = self.range
                middle = _range[1]+((_range[0]-_range[1])//2)
                return middle, middle
            elif len(self._nodes) == 1:
                only_one = self._nodes[0].category
                return only_one, only_one
            else:
                return self._actual_highest, self._actual_lowest

        def _recalc_highest_and_lowest(self):
            _l = 160
            _h = 0
            for n in self._nodes:
                node = self._nodes[n]
                cat = node.category
                if cat < _l:
                    _l = cat
                if cat > _h:
                    _h = cat
            self._actual_highest = _h
            self._actual_lowest = _l

        def split(self):
            highest, lowest = self.highest_and_lowest
            if len(self._nodes) < 4: #we want a minimum of 4 nodes before we do a split.
                return self, None
            elif highest-lowest < 1: #they are all of the same category? Cant split that.
                return self, None
            split_mark = lowest+((highest-lowest)//2)
            new_bucket = NodeManager.Bucket(split_mark, self._lower_cat, smallest=self.smallest)
            self._smallest = False
            self._lower_cat = split_mark+1
            to_delete = []
            for n in self._nodes:
                node = self._nodes[n]
                c = node.category
                if c <= split_mark:
                    new_bucket.append(node)
                    to_delete.append(n)
            for _id in to_delete:
                del self._nodes[_id]
            self._recalc_highest_and_lowest()
            return self, new_bucket

        def __init__(self, upper_cat, lower_cat, smallest=False):
            self._smallest = smallest
            self._upper_cat = upper_cat
            self._lower_cat = lower_cat
            self._actual_lowest = 160
            self._actual_highest = 0
            self._nodes = {}

    @classmethod
    def uncompact_nodes(cls, _id, nodes):
        node_objects = []
        count = len(nodes) // 26
        for i in range(count):
            nodestring = nodes[i * 26:(i + 1) * 26]
            other_id = nodestring[0:20]
            ip_addr = nodestring[20:24]
            port = nodestring[24:26]
            ip_addr = struct.unpack("!BBBB", ip_addr)
            ip_addr = '.'.join((str(ip_part) for ip_part in ip_addr))
            port = struct.unpack("!H", port)[0]
            new_node = NodeManager.OtherNode(_id, other_id, ip_addr, port)
            node_objects.append(new_node)

        return node_objects

    @classmethod
    def uncompact_ip_values(cls, _id, values):
        ip_addr_dicts = []
        for v in values:
            ip_addr = v[0:4]
            port = v[4:6]
            ip_addr = struct.unpack("!BBBB", ip_addr)
            ip_addr = '.'.join((str(ip_part) for ip_part in ip_addr))
            port = struct.unpack("!H", port)[0]
            new_ip_addr = {"ip_address": ip_addr, "port": port}
            ip_addr_dicts.append(new_ip_addr)
        return ip_addr_dicts


    @asyncio.coroutine
    def node_bucket_manager(self):
        def manage():
            created = []
            for bucket in self.buckets:
                if len(bucket) > MAX_NODES_PER_BUCKET:
                    if bucket.smallest:
                        existing, new_bucket = bucket.split()
                        if new_bucket is not None:
                            created.append(new_bucket)
                    else:
                        pass  # TODO: replace oldest nodes with newer ones.

            for c in created:
                self.buckets.append(c)
        while 1:
            try:
                print("Waiting for buckets_changed_event.")
                yield from self.buckets_changed_event.wait()
                print("Received buckets_changed_event!")
                with (yield from self.buckets_lock):
                    manage()
                    if self.buckets_changed_event.is_set():
                        self.buckets_changed_event.clear()
                    print("Finished managing buckets.")
            except Exception as e:
                print(e.args[0])
                break


    @asyncio.coroutine
    def add_nodes_to_buckets(self, client, nodes, ping_before_add=False):
        if ping_before_add is not False:
            to_mark = []
            loop = asyncio.get_event_loop_policy().get_event_loop()  # TODO use existing loop here.
            tasks = (
                (lambda _n: loop.create_datagram_endpoint(
                lambda: ping_before_add(client, other_id=_n.id, timeout=3),
                remote_addr=(_n.ip_address, _n.port)))(n) for n in nodes
            )
            task_tps = yield from asyncio.gather(*tasks)
            tasks = [p.on_completed.wait() for _, p in task_tps]
            yield from asyncio.gather(*tasks)
            for _, p in task_tps:
                if p._timed_out or p._errored:
                    to_mark.append(p._other_id)

            for node in nodes:
                if node.id in to_mark:
                    del node

        print("Need to add nodes to buckets. Trying for lock.")
        with (yield from self.buckets_lock):
            print("Got buckets lock.")
            for n in nodes:
                cat = n.category
                for b in self.buckets:
                    upper, lower = b.range
                    if cat <= upper and cat >= lower:
                        b.append(n)
                        break
        print("Unlocked buckets_lock. Setting the bucket_changed_event.")
        self.buckets_changed_event.set()

    def __init__(self, loop):
        self.buckets_lock = asyncio.Lock(loop=loop)
        self.buckets_changed_event = asyncio.Event(loop=loop)
        self.buckets = [NodeManager.Bucket(160, 160), NodeManager.Bucket(159, 0, smallest=True)]
