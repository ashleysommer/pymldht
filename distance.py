#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pymldht - An open-source Python 3.5+ Mainline DHT client implementation
(c) September 2016, Ashley Sommer
Release under the MIT Open-Source Software licence. See licence.md for full licence text.
"""


def calculate_distance(id1, id2):
    assert len(id1) == 20
    assert len(id2) == 20
    distance = bytearray(20)
    for i in range(20):
        distance[i] = id1[i] ^ id2[i]
    return bytes(distance)


def categorize_distance(distance):
    assert len(distance) == 20
    # largest (most-significant byte first)
    category = 160
    f_byte = 0x80
    for i in range(20):
        check_byte = distance[i]
        if check_byte == 0x00:
            category -= 8
            continue
        for j in range(8):
            if check_byte & (f_byte >> j):
                break
            else:
                category -= 1
        else:
            continue
        break
    return category
